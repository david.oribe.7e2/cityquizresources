package cat.itb.cityquiz.domain;

import android.os.SystemClock;

import java.util.ArrayList;
import java.util.List;

public class Game {
    public static final int maxQuestions = 5;
    public static final int possibleAnswers = 6;
    public static final int MAX_MILIS_PER_ANSWER = 5000;
    public static final int MAX_SCORE = 100;

    List<Question> questions;
    List<Answer> answers;
    long startTime;

    public Game(List<Question> questions) {
        this.questions = questions;
        this.answers = new ArrayList<>();
        this.startTime = SystemClock.elapsedRealtime();
    }

    public int getNumCorrectAnswers() {
        int score = 0;
        for(int i=0; i<answers.size(); i++){
            if(questions.get(i).getCorrectCity().equals(answers.get(i).city)){
                score++;
            }
        }

        return score;
    }

    public int getScore() {
        int maxScorePerQuestion = MAX_SCORE / maxQuestions;

        int score = 0;
        long lastTime = startTime;
        for(int i=0; i<answers.size(); i++){
            Answer answer = answers.get(i);
            if(questions.get(i).getCorrectCity().equals(answers.get(i).city)){
                long elapsedTime = answer.time - lastTime;
                long leftTime = MAX_MILIS_PER_ANSWER - elapsedTime;
                int currentScore = (int)leftTime*maxScorePerQuestion/MAX_MILIS_PER_ANSWER;
                score += currentScore;

            }

            lastTime = answer.time;
        }

        return score;
    }

    public Question getCurrentQuestion(){
        if(getNumAnsweredQuestions()>=questions.size()) return null;
        return questions.get(getNumAnsweredQuestions());
    }

    private int getNumAnsweredQuestions() {
        return answers.size();
    }


    public void questionAnswered(City city) {
        answers.add(new Answer(city));
    }

    public boolean isFinished() {
        return getNumAnsweredQuestions() >= maxQuestions;
    }

    public boolean isReadyToStart() {
        return getNumAnsweredQuestions()==0 && questions!=null && questions.size()>0;
    }

    public void skipQuestion() {
        questionAnswered(null);
    }
}
