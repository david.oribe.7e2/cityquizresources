package cat.itb.cityquiz.presentation.screens.welcome;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.screens.gamequiz.QuizViewModel;


public class WelcomeFragment extends Fragment {

    private QuizViewModel mViewModel;



    public static WelcomeFragment newInstance() {
        return new WelcomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.welcome_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        mViewModel.getGame().observe(this, this::onGameChanged);
    }

    private void onGameChanged(Game game) {
        if(game!=null){ //SI EL JUEGO QUE RECIBE NO ES NULO NAVEGA CON LA FLECHA PARA VOLVER A EMPEZAR
            Navigation.findNavController(getView()).navigate(R.id.go_to_quiz_action);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.btnStart)
    public void onViewClicked(){
        mViewModel.createGame(); //Crea otro juego
    }



}
