package cat.itb.cityquiz.presentation.screens.gamequiz;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;

public class QuizFragment extends Fragment {

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.answer1)
    Button answer1;
    @BindView(R.id.answer2)
    Button answer2;
    @BindView(R.id.answer3)
    Button answer3;
    @BindView(R.id.answer4)
    Button answer4;
    @BindView(R.id.answer5)
    Button answer5;
    @BindView(R.id.answer6)
    Button answer6;
    private QuizViewModel mViewModel;
    private Game game;
    private int score;

    public int getScore() {
        return score;
    }




    public static QuizFragment newInstance() {
        return new QuizFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.quiz_fragment, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
//        game = mViewModel.getGame().getValue();
//        display(game, getContext());
        mViewModel.getGame().observe(this, this::onGameChanged);
    }

    private void onGameChanged(Game game) {
        if (!game.isFinished()) { //crea un nuevo juego si el juego no ha acabado (5 veces)
            display(game);
        }else{
            Navigation.findNavController(getView()).navigate(R.id.go_to_finished_fragment); //Cuando acaba pasa al siguiente fragment
        }
    }


    private void displayButtons(Game game) { //Método para meter el texto en cada botón
        Question question = game.getCurrentQuestion();
        answer1.setText(question.getPossibleCities().get(0).getName());
        answer2.setText(question.getPossibleCities().get(1).getName());
        answer3.setText(question.getPossibleCities().get(2).getName());
        answer4.setText(question.getPossibleCities().get(3).getName());
        answer5.setText(question.getPossibleCities().get(4).getName());
        answer6.setText(question.getPossibleCities().get(5).getName());

    }

    private void displayImage(Game game, Context appContext){ //Método para meter la imagen en el ImageView
        Question question = game.getCurrentQuestion();
        String fileName = ImagesDownloader.scapeName(question.getPossibleCities().get(0).getName());
        int resId = appContext.getResources().getIdentifier(fileName, "drawable", appContext.getPackageName());
        imageView.setImageResource(resId);

    }

    public void display(Game game){
        displayButtons(game);
        displayImage(game, getContext());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }


    @OnClick({R.id.answer1, R.id.answer2, R.id.answer3, R.id.answer4, R.id.answer5, R.id.answer6})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.answer1:
                answerQuestion(0);
                break;
            case R.id.answer2:
                answerQuestion(1);
                break;
            case R.id.answer3:
                answerQuestion(2);
                break;
            case R.id.answer4:
                answerQuestion(3);
                break;
            case R.id.answer5:
                answerQuestion(4);
                break;
            case R.id.answer6:
                answerQuestion(5);
                break;
        }
    }

    private void answerQuestion(int i) {
        mViewModel.answerQuestion(i);
    }



}
