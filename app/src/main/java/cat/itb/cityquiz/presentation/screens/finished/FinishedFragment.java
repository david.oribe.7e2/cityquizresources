package cat.itb.cityquiz.presentation.screens.finished;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.screens.gamequiz.QuizFragment;
import cat.itb.cityquiz.presentation.screens.gamequiz.QuizViewModel;

public class FinishedFragment extends Fragment {

    @BindView(R.id.scoreText)
    TextView scoreText;
    private QuizViewModel mViewModel;
    private Game game;
    private QuizFragment quizFragment;


    public static FinishedFragment newInstance() {
        return new FinishedFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.finished_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        mViewModel.getGame().observe(this, this::onGameChanged);
    }

    private void onGameChanged(Game game) {
        if(game.isFinished())
            displayScore(game);
        else
            Navigation.findNavController(getView()).navigate(R.id.go_to_quiz_again);
    }

    private void displayScore(Game game) {
        scoreText.setText(String.valueOf(game.getNumCorrectAnswers())); //Set de las preguntas acertadas

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }


        @OnClick(R.id.btnPlayAgain)
        public void onViewClicked() {
            mViewModel.createGame(); //CREA OTRO JUEGO

        }
    }

