package cat.itb.cityquiz.presentation.screens.gamequiz;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class QuizViewModel extends ViewModel {
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    MutableLiveData<Game> game=new MutableLiveData<>();

    public MutableLiveData <Game> getGame() {
        return game;
    }

    public void createGame(){
        Game newGame =gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
        game.postValue(newGame);
    }

    public void answerQuestion(int btnOption){
        Game newGame = gameLogic.answerQuestions(game.getValue(), btnOption);
        game.postValue(newGame);

    }

}
