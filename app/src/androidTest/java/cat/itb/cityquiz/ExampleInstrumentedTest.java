package cat.itb.cityquiz;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;



import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.cityquiz.presentation.WelcomeActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

/**
 * Instrumented createGame, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<WelcomeActivity> mActivityTestRule = new ActivityTestRule<>(WelcomeActivity.class);

    @Test
    public void checkButtons(){
        onView(withId(R.id.btnStart)).perform(click()); //click en el botón indicado
        onView(withId(R.id.imageView)).perform(click()).check(matches(isDisplayed())); //Exists image
        onView(withId(R.id.answer5)).perform(click());
        onView(withId(R.id.answer4)).perform(click());
        onView(withId(R.id.answer3)).perform(click());
        onView(withId(R.id.answer2)).perform(click());
        onView(withId(R.id.answer1)).perform(click());
        onView(withId(R.id.btnPlayAgain)).perform(click());


    }
}
